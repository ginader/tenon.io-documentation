#Understanding Tenon API Request Parameters

This document discusses all of the various request parameters available to Tenon API users. The number and kinds of request parameters we provide allows Tenon to work for you in a way that matches your specific goals and needs.

There are three types of parameters described below:
1. Required parameters
2. Optional parameters
3. Paid-only parameters

##Required Parameters
There are only two required parameters: 'key' and *either* 'url' or 'src'.

###key

The `key` parameter is the API key you received upon registering. The key is required for every API request you send. Missing or invalid keys will result in error response. 

###url *or* src

Either the `url` or `src` are required for testing.  The URL must be a fully qualified URL that the API has access to. If any `GET` parameters are required in order to access the URL, you must supply them as part of the URL.  If using `src` it must be a string of document source only (HTML, CSS, and JavaScript).

Note: Supplying both `url` and `src`, beyond being plain silly, will result in only the URL being tested. 

When supplying `src`, this can be anything ranging from the source of a complete document or only a fragment of a document (mark this case using the `fragment` parameter, described below). The `src` is preferable when testing something that is otherwise not available via URL.  

If using `src`, ensure that the path to all document assets is set to a fully qualified path, otherwise those assets (and their impact on the UI) will not be accurately tested. For instance, if you're testing a document fragment that consists of a JavaScript-driven widget, you'll need to also do one of the following:

* include either the JavaScript and CSS source in the document itself
* include the fully qualified path of the relevant assets in the relevant `<script>` and `<link>` tags of the source string 
* or, send over the rendered document source in its state after the JavaScript 
 
*Remember: the important part of testing is to test that which will be experienced by the user which means, if testing document source, you should send over a "rendered" version of the source*

Note: If you plan to send over document source, you may want to explore utilizing a Grunt plugin like [Grunt HTML Smoosher](https://www.npmjs.org/package/grunt-html-smoosher) to help prepare the source.

## Optional Parameters

*All* of the below parameters are optional for all users.

Some of these parameters may not be available to you depending upon your plan. For specific details on what your plan allows, review your plan details.

Providing any value outside of the accepted list of values will result in failure

###certainty

Sometimes accessibility tools will return results that the tool isn't completely sure is a real issue. The `certainty` parameter allows you to filter out these "uncertain" results by choosing a minimum acceptable certainty score. 

* Valid values: must be one of '0','20','40','60','80','100'
* The default value is '0', meaning that the minimum certainty score for issues returned is '0'

Note: You probably don't want to only choose '100' as this will, in practice, probably be too conservative. We try to create tests that return real results and so very low certainty scores should be few and far between. As a general guide, if integrating Tenon into an automated build or pre-commit hook, use 80 and above. For a QA scenario, choose the lowest value you can tolerate as an organization.

###fragment

The `fragment` parameter only works when submitting content to the API via the `src` attribute. Using `fragment` when testing an url will be ignored.  This parameter allows you to identify the source string as only a fragment of a page.  This will essentially filter out the reporting of document-level issues and return results relevant only to the source fragment you submit.

* Valid values: must be one of '0','1' representing false and true.
* The default value is '0'

Note: do not set `fragment` to '1' if you're sending over a complete document. You *will* get weird results.


###importance

The importance parameter allows you to indicate a relative importance of this specific document against others you may be testing.  For instance, the site's home page or contact page may be the most important in your site and thus be rated as "High" (indicated by '3') importance. This allows issues on important pages to "bubble up" compared to the issues on other pages. Therefore, this is only relevant when testing multiple pages.

* Valid values: must be one of '0','1','2','3' representing "None", "Low", "Medium", and "High".
* The default value is '0', meaning that the minimum importance used in calculating final relative priority for issues returned is '0'

This parameter is used when calculating final issue priority in the results.  The `importance` parameter is only really relevant or useful when compiling a report set consisting of multiple documents. By doing so, the issues on the *more* important document will be correspondingly more important than those on other documents that were tested.

###level

The level parameter indicates the "lowest" WCAG level to test against.

* Valid values: must be one of 'A','AA','AAA'.  Chosing "AA", for instance, will test both "AA" and "A" success criteria. 
* Default value is 'AAA', meaning that all tests for AAA, AA, and A will be run. 

Note: Keep in mind that WCAG Level does not actually correlate to Priority when determining what to test for.  For more information on our philosophy regarding prioritization, see blog post by Karl Groves: [Understanding WCAG Level](http://http://www.karlgroves.com/2013/05/20/understanding-wcag-level/)

In practice, very few of the Level AAA Success Criteria are testable using automated means and therefore you'll see few tests against these anyway. Despite this, we recommend selecting "AAA" for the `level` parameter and being more stringent in your `certainty` setting, because it is likely that doing so will have a greater impact against False Positives than filtering by Level.

###priority

Each Best Practice in the system has a number of factors that allow us to determine their relative priority. In your result set, each issue reported will be given a calculated priority score which you can then use to filter or order your results.

* Valid values: must be one of '0','20','40','60','80','100'
* Default value will be '0'. As a consequence all issues, regardless of Priority, will be reported.

Note: in practice, it is unlikely that an issue returned will have 100% priority. Like `certainty`, you should supply the lowest priority you can, especially when first starting out. Setting this too high may result in no errors returned.   

###ref

The `ref` parameter indicates whether or not you wish to be given a link with each issue which includes reference information for the violated Best Practice.  This reference information can be immensely useful for your team if your organization is new to accessibility, as the URL provided will supply extensive details on the issue as well as remediation guidance.

* Valid values: must be one of '0','1' representing 'false' and 'true'.
* Default value is '1', meaning that each issue reported will also have an associated URL at which you can find reference material.

Note: you can greatly diminish the size of the JSON response by passing '0' for this parameter.


###reportID

The `reportID` parameter is a string of text you can supply to identify the request as part of a specific report. This allows the requests to be grouped together as though they were part of the same test effort.

* Valid values: can be any arbitrary alphanumeric string up to 255 characters in length.

Note: it is your responsibility to ensure accuracy and validity of the supplied string. We will accept any (safe) arbitrary string for this parameter. If no value is supplied, we will create one.

###store

The `store` parameter indicates whether or not Tenon should store your results. You can then come back later and view your results in our Web-based system.

* Valid values: must be one of '0','1' representing 'false' and 'true'.
* Default value is '0', meaning that we will not store the results.

Note: The availability of this parameter and length of time we will store the results depends upon your plan.


###systemID

The `systemID` parameter is a string of text you can supply to identify the tested document as part of a specific system. This can (and should) be used in conjunction with the reportID parameter so that you can identify your request as being part of a test effort (a report) on a specific system.  This is especially useful if you are developing or testing multiple projects.

* Valid values: can be any arbitrary alphanumeric string up to 255 characters in length.

Note: it is your responsibility to ensure accuracy and validity of the supplied string. We will accept any (safe) arbitrary string for this parameter. If no value is supplied, we will create one.


###uaString

The `uaString` parameter allows you to supply any arbitrary string of text which will be sent as the User-Agent string when we request the URL you supply. This is particularly useful if your site does any user-agent sniffing.

* Valid values: can be any arbitrary string up to 255 characters in length.

Note: it is your responsibility to ensure accuracy and validity of the supplied string. We will accept any (safe) arbitrary string for this parameter. If no value is supplied, we will create one.


###viewPortHeight

The `viewPortHeight` parameter sets the height, in pixels, of the viewport for our headless browser. 

* Valid values: must be a string consisting only of numeric characters, no more than four characters in length.  In other words, it can be any positive number between 0 and 9999.

Note: If this parameter it is not supplied at time of request it will be set to '768'. *If you set one viewport parameter you must set both*


###viewPortWidth
The `viewPortWidth` parameter sets the width, in pixels, of the viewport for our headless browser. This parameter is particularly useful when testing the accessibility of your responsive design. 

* Valid values: must be a string consisting only of numeric characters, no more than four characters in length.  In other words, it can be any positive number between 0 and 9999. 

Note: If this parameter it is not supplied at time of request it will be set to '1024'. *If you set one viewport parameter you must set both*