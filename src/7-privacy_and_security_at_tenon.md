#Privacy and Security at Tenon.io

Last Updated May 29, 2014

## Introduction

Tenon takes privacy and security of customer data very seriously. We also take transparency very seriously. The following document discusses our approaches to Privacy and Security.


## Third-Parties

No customer information will be shared with any third party except as needed to provide quality customer service.  Tenon makes use of the following third party services for some of our functionality and/ or customer service activities, each of whom have their own privacy policies to which we link below. 

* Customer support via [Desk.com](http://www.desk.com/privacy)
* E-mail announcments, email marketing via [MailChimp](http://mailchimp.com/legal/)
* Payment processing via [Stripe](https://stripe.com/us/terms)
* Analytics via [Google Analytics](http://www.google.com/intl/en/policies/privacy/)

Tenon is not responsible for Privacy or Security problems derived from third parties. We encourage you to read and understand the above third parties' policies as well as ours.

## Tenon Itself

### General

1. All traffic to Tenon API, Tenon website, and any other content or services available on the tenon.io domain uses HTTPS protocol
2. At no time do we store any credit card data on our servers.
3. All passwords stored in our database are hashed with a one-way hash. Our forgotten password process is actually a reset process. Not even Tenon.io staff can see your password in plaintext.
4. We routinely test our system for common security vulnerabilities such as SQL Injection, XSS, CSRF, and more.

### Cookies

We set cookies for the following reasons on tenon.io:

1. Whenever someone uses the public demo
2. Whenever someone lands on a landing page from a CPC ad, email newsletter, or other marketing asset.
3. Whenever a registered user logs into the Tenon.io website

Use of any web system under the tenon.io domain is implicit consent to accept cookies.

### Public Demo

1. The public demo page stores all results temporarily as described below.
2. The public demo also stores the requesting computer's IP address and sets a cookie.

We do not have any display ads or any other code which would use a beacon or set a third-party cookie.

### Storage of test data

1. We temporarily store every result of every test run by the tenon API. The length of time this is stored depends largely upon server demand but is anticipated to be less than a few minutes.
2. We permanently store your test results if:
   * Your plan allows the `'store'` parameter, and;
   * You specifically set that parameter to `'1'`
3. In both of the above cases, the information stored is the verbatim copy of the JSON response, stored as a string.
4. If your API request is for testing of document source (via the `'src'` request parameter) we will temporarily store that document source for the purposes of testing. After testing is complete we delete the source.[1]
5. If your API request calls for testing of an URL, we do not store the source of that document in any way.[1]
6. We calculate statistics for a wide array of things such as document size, number of errors, and types of errors and we store this information permanently. Some of this informatiom is associated with your account, for the purposes of presenting this useful information to you. 
7. We store messages sent to us by our payment processor that relate to your account. At no time does this contain any PII or credit card data. Instead, our processor sends us unique IDs which can be referenced later so that we can manage your account without requiring direct access to your information


## Footnotes
1. We may, in the future, choose to store tested document source and/ or a snapshot image of the tested page. This would occur as part of future visualization features.