#Tenon.io Quick Start

## Quick note:

Tenon is currently in private beta. This documentation discusses registering to get an API key, which is currently inaccurate.
While in private beta, you can request an API key by doing the following:

* Go to [http://www.tenon.io](http://www.tenon.io) to get on the mailing list for new beta announcements
* AND email karl@tenon.io directly to request the key.

## Intro 

To see Tenon in action and to verify it doesn't suck: take a quick trip to:
[http://tenon.io/#testnow](http://tenon.io/#testnow)

Now, let's dive in:

Reading endless documentation is no fun. Let's get started:

1. You need an API key. If you don't have one,  go to http://www.tenon.io/register.php (*See note above*)
2. Some things to know in advance:
   1. The API URL is http://www.tenon.io/api/
   2. You'll be sending a POST request to that URL. GET requests get no response.
   3. The only required parameter is 'key'. Put your API key here
   4. You must also supply either an 'url' parameter or a 'src' parameter. Their names are self-explanatory: the 'url' is the address of a publicly reachable URL. The 'src' is the full document source for a page you want tested.
   5. All other parameters are optional so we aren't talking about them here.
   
## Example requests

Below we show a handful of examples for submitting a request to the API. All examples below show submitting a request to test an URL.  Note: none of these are "production-ready" code. You'll need to customize them to make them your own.

### cURL
The single easiest way to test the API is from command line, using cURL to POST your request:

```
$ curl -X POST -H Content-Type:application/x-www-form-urlencoded -H Cache-Control:no-cache -d 'url=URL-TO-TEST&key=PUT-YOUR-KEY-HERE' http://www.tenon.io/api/'
```

Naturally, in the above, you'd replace `URL-TO-TEST` with the URL you want to test and you'd replace `PUT-YOUR-KEY-HERE` with your API key.

As you'll see by running thos command, the response is JSON-formatted and therefore it is not very useful to use Tenon strictly from the command line, but you get the point. 

### PHP & cURL

A more useful approach would be to POST the request using PHP and cURL. The below example shows getting the JSON response as a variable named `$result`.


```
$opts['key'] = 'YOUR API KEY GOES HERE';
$opts['url'] = 'YOUR URL TO TEST GOES HERE';

// open connection
$ch = curl_init();

// set our curl options
curl_setopt($ch, CURLOPT_URL, 'http://www.tenon.io/api/');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_FAILONERROR, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $opts);

//execute post and get results
$result = curl_exec($ch);

//close connection
curl_close($ch);

//this convers the JSON API response to a PHP array
$result = json_decode($result, true);

//now do something useful with the array of data

```

### Python 2 & urllib

```
import urllib
params = urllib.urlencode({'url': 'PUT-YOUR-URl-HERE', 'key': 'PUT-YOUR-KEY-HERE'})
f = urllib.urlopen("http://www.tenon.io/api/", params)
print f.read()
```

### Python 3 & urllib

```
import urllib.request
import urllib.parse
data = urllib.parse.urlencode({'url': 'PUT-YOUR-URL-HERE', 'key': 'PUT-YOUR-KEY-HERE'})
data = data.encode('utf-8')
request = urllib.request.Request("http://www.tenon.io/api/")
# adding charset parameter to the Content-Type header.
request.add_header("Content-Type","application/x-www-form-urlencoded;charset=utf-8")
f = urllib.request.urlopen(request, data)
print(f.read().decode('utf-8'))
```


### Node.js

```
var querystring = require('querystring');

var data = querystring.stringify({
      url: PUT-YOUR-URL-HERE,
      key: PUT-YOUR-KEY-HERE
    });

var options = {
    host: 'www.tenon.io',
    port: 80,
    path: '/api/',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(data)
    }
};

var req = http.request(options, function(res) {
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
        console.log("body: " + chunk);
    });
});

req.write(data);
req.end();
```
Naturally you'll want to do something more useful with this example than simply logging it to console.

## Dealing with the response

The methods above will return a JSON response which you'll then need to do something useful with. Read 'Overview of the Tenon API Response' elsewhere in these docs to get an overview of the exact response format.

## Additional parameters

There are a variety of additional parameters that can be submitted with your request. Read 'Understanding API Request Parameters' to see what they are.