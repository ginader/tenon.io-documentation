# Tenon.io API Developer Documentation

The following information describes the Tenon.io API, including all actions that can be taken using the API, all parameters (required and optional), and all response codes that are returned.

## Introduction
Tenon.io is a web-based application and API that facilitates the testing of web-based products and services for compliance with major standards for Accessibility.

## Abuse
Every single request against this API is tracked by our system.  Any abusive behavior *will*  cause an immediate and irreversible revocation of your privileges to use this API. For some types of abuse this will happen at the very first sign of abuse, with no warning whatsoever.

## Obtaining Access to the API

*Please see note in Tenon Quickstart*

To obtain access to the API you must register at [http://www.tenon.io/register.php](http://www.tenon.io/register.php).  Upon successfully registering for the API, you'll receive an email in which you will find an URL to confirm your registration. After clicking on the confirmation link, you will be set up with an API key.

## Using the API
Using the API is dead simple: send a POST request to the API with your API key and an URL and we shoot back results.  There are also a number of additional parameters you can use to fine-tune what types of errors are returned. 

### API Request/ Response overview
At a high level, here's how Tenon works:

1. Your client application submits a request against the API
2. Tenon receives the request and validates it
3. If the request passes validation, Tenon creates a new record in the database, associated with your account
4. Tenon runs its tests against the URL or document source
5. Tenon returns a JSON response and also stores the response in the database.

### Submitting Requests

* The URL to submit your requests is http://tenon.io/api
* All requests must be POST requests.  GET requests will be rejected entirely without a meaningful response.
* All requests must contain API key and either document source as a string or an URL to a publicly available URI. The API validates these parameters and, if they're invalid or out of bounds, processing will stop and you will get an error response.
* A full list of request parameters can be found in "Understanding API Request Parameters"

### Responses
Response will be returned to you in the form of JSON. JSON is the only format we use. The JSON string will be formatted as follows for all responses. For error responses the resultSet node will not exist. For a full description of the API response, see "Overview of the Tenon API Response".

## Notes For Your Success
  * All tests contain what we calla a 'certainty' score. This is described in both "Understanding API Request Parameters" and "Overview of the Tenon API Response".  Items with a low certainty score are likely to become “white noise” in the report output. On a practical level you may want to accept only items with a high certainty score.  Be careful that you may be filtering valid results.
  * We do not recommend that you cache or store the error information (title, description, etc.) because it’s highly likely that the tests, titles, and descriptions will vary and evolve over time.  As we develop and improve the testing and reporting this information is also likely to improve.  Storing it will mean you don’t have the latest and best information.

### Testing Document Source vs. URL
The Tenon.io web service is JavaScript-aware. This means that it uses a headless browser to do its testing. This allows it to be highly accurate when testing interfaces that use JavaScript to generate or modify interface elements.  To get the most out of the Tenon.io web service, you should submit an URL to a publicly accessible location. This will ensure that Tenon.io can perform the most accurate "real world" testing. This may be difficult for testing pages in a development environment which is behind a firewall, VPN, or other environment that requires authentication. The Tenon.io service is capable of testing document source. It will take the string that you supply in the 'src' parameter of the request, turn it into a web page, and test that. If you submit document source, it would be best to ensure that source paths to JavaScript and CSS files be accessible and coded in a way that allows the API to get to it. One way of ensuring this is to use CDN hosted versions of your files. Alternately, you can send over the final rendered version of the page as document source. The more closely your source reflects the final rendered DOM of the page, the better. 

Remember, a publicly accessible URL is always best because it allows us to test the real page as it would be experienced by the user.

## Getting the most out of the API
  * **Test early, test often.** Accessibility issues are best if they're avoided entirely, so test your code as you develop, not after it is already deployed. Post deployment remediation is much more expensive than avoiding the problem in the first place. This is the entire reason why we've created an API rather than a fully-featured standalone system.
  * **Tweak your settings.**  Experiment with optional parameters like 'certainty', 'priority', and 'level' to make sure you are getting useful and accurate results.  Its our goal to make sure we're only providing useful and accurate results but sometimes context matters. If you find that you're getting a lot of false positives, let us know.
  * **Speed up your site.** In our own testing, almost all instances where we throw a timeout error it is because the URL being tested is slow.  If you get a timeout error, test your site with YSlow. If you get anything other than a 'D' or 'F' from YSlow, email us.
