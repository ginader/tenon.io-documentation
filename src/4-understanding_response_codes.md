#Understanding Response Codes from Tenon API

Tenon API uses HTTP Status codes to indicate whether your request was a success or failure. We also provide some additional information useful in determining the nature of the failure.  The following example illustrates the relevant portion of our JSON response at which you'll locate this information:

* **status:** the HTTP status code
* **message:** The associated message for the code, per [RFC 2616](http://tools.ietf.org/html/rfc2616)
* **code:** a token string showing what the response was
* **moreInfo:** an URL to a more detailed explanation of the code

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "invalid_param",
        "moreInfo": "https://www.tenon.io/docs/invalid_param";
    }
}
```

##Success Codes

### 'success'

The 'success' codes is easy to understand. This code means all went well with the request. All required parameters were set, the API key was valid, the supplied parameters were valid, the test document was reachable, and we were able to successfully perform our testing and return a response.

####Example:

```
{
    "response": {
        "status": "200",
        "message": "OK",
        "code": "success",
        "moreInfo": "https://www.tenon.io/docs/success";
    }
}
```

## Failure Codes

### Service Issues

The following failure codes represent that something went wrong on our end. Performance of our service is highly important to us. If you experience repeated performance issues please contact us so that we may investigate what is happening.

#### 'exception'

This means an exception happened on our end. Our system is currently running at 94% success rate, meaning 94% of requests run without issue. If you get a 500 error, chances are, you can re-run your request without issue, so we recommend that in your final implementation, if you get a 500 error, retry once or twice. Re-running more than twice is a bad idea because, in our own use of the system we never see more than two 500s in a row unless something is seriously wrong. 

#####Example

```
{
    "response": {
        "status": "500",
        "message": "Internal Server Error",
        "code": "exception",
        "moreInfo": "https://www.tenon.io/docs/exception";
    }
}
```

#### 'request_timeout'

This means that the overall process took longer than we allow.  The average successful response time is between 4 - 7 seconds for most requests - that's from the time you submit your request to the time we return a response.  We actually wait much longer for the process to complete, so if you get this response, something went wrong. However, ** the issue may be on your side ** Request timeouts tend to happen for one of three reasons:

* Something's wrong with your JavaScript: either your JavaScript has uncaught exceptions or your page never fires the onload event.  In the latter case, we attempt to overcome this on our end but sometimes it just doesn't workout and we just kick it back. 
* Your page includes third party content or features that never finish loading.  Related to the above, sometimes timeouts are caused by third party stuff that never finishes loading
* Your page's performance is too slow.
* Your page's accessibility issues are so numerous that testing & processing takes so long that the process times out. This is actually a severe edge case, as we've seen pages with as many as 1800 errors that still responded, so this is far less likely than the other two cases, but still probable.

The issue in your case may be one of the above or a combination of the above. Regardless, we recommend investigating what the issue may be. This is especially true if you get multiple 522 responses. While a single 522 response here and there likely resides on our end, multiple 522 responses for multiple pages on the same site is likely to mean something to do with your code. In that case, you should investigate page performance, looking specifically for uncaught exceptions in your JavaScript.

#####Example

```
{
    "response": {
        "status": "522",
        "message": "Connection Time Out",
        "code": "request_timeout",
        "moreInfo": "https://www.tenon.io/docs/request_timeout";
    }
}
```

### Request Issues

400 & 401 status responses are likely to be the most common failure responses you'll find and are due to issues with your request. 

#### api_credentials_invalid

User supplied an invalid API key

#####Example

```
{
    "response": {
        "status": "401",
        "message": "Unauthorized",
        "code": "api_credentials_invalid",
        "moreInfo": "https://www.tenon.io/docs/api_credentials_invalid";
    }
}
```

#### daily_limit_reached

User submitted more requests today than their plan's daily limit allows.

#####Example

```
{
    "response": {
        "status": "401",
        "message": "Unauthorized",
        "code": "daily_limit_reached",
        "moreInfo": "https://www.tenon.io/docs/daily_limit_reached";
    }
}
```

####blank_url_or_src

The parameter supplied for ‘src’ was blank/ missing and the parameter supplied for 'url' was also blank.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "blank_url_or_src",
        "moreInfo": "https://www.tenon.io/docs/blank_url_or_src";
    }
}
```

####bad_src

The parameter supplied for ‘src’ was not HTML.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "bad_src",
        "moreInfo": "https://www.tenon.io/docs/bad_src";
    }
}
```

####invalid_param

User supplied a parameter value that is outside of the expected range.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "invalid_param",
        "moreInfo": "https://www.tenon.io/docs/invalid_param";
    }
}
```

####abuse_detected

User attempted to submit an abusive request to the system.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "abuse_detected",
        "moreInfo": "https://www.tenon.io/docs/abuse_detected";
    }
}
```


####required_param_missing

A required parameter was omitted from the request

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "required_param_missing",
        "moreInfo": "https://www.tenon.io/docs/required_param_missing";
    }
}
```

####doc_source_too_big

The size of the document submitted was too big.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "doc_source_too_big",
        "moreInfo": "https://www.tenon.io/docs/doc_source_too_big";
    }
}
```

####improper_content_type

The content type detected was not a web page.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "improper_content_type",
        "moreInfo": "https://www.tenon.io/docs/improper_content_type";
    }
}
```

####url_request_failed

The URL supplied did not return a good HTTP response.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "url_request_failed",
        "moreInfo": "https://www.tenon.io/docs/url_request_failed";
    }
}
```
