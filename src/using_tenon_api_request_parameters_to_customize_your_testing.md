# Using Tenon API Request Parameters to Customize your Testing

## I want to test only a portion of the page, not the whole thing

## I want to avoid false positives

## I want to know what my biggest problems are

## I want to test my responsive design