#Tenon Roadmap

The Tenon Roadmap offers a glimpse into where we're going in the future. This document is neither complete nor definitive and is very likely to evolve based on demand. If you are a Tenon.io customer, give us your feedback on what you'd like to see and what you think is most important.

Last Update: May 29, 2014

## Version 1.1

The theme for 1.1 will be better stats, better tests, and increased preparation for Internationalization.

* By calculating better & more informative stats, we can offer a better glimpse into your systems' accessibility and make more informed development decisions
* Nearly 60 new tests will be added, increasing coverage for ARIA and more JavaScript-aware testing
* In terms of internationalization, nearly all of Tenon's UI strings are already translated into 8 other languages.  Additional work is needed, after which we will gauge further content translation based on demand.

The following outlines some specific new features in three different categories.

### Tenon Web Admin

* Enhanced Stats
  * Top X issues by Certainty (TEN-312)
  * % of issues by Certainty (TEN-313)
  * Top X issues by WCAG Level (TEN-311)
  * % of issues by WCAG Level (TEN-310)
  * Num. of issues by WCAG Level (TEN-309)
  * Avg. Certainty (TEN-305)
  * Avg. Priority (TEN-306)
* Add preferred language to user profile. This is a dependency for i18N work (TEN-350)
* Create an "All Best Practices" report for internal use only. The use case is to provide an ability to get a look at all BPs at once, to check for duplicates, etc. (TEN-29)
* In stats output, also provide a version that includes only today's info (TEN-340)
* Drive all emails sent by Tenon via templates. This is an important component of i18N (TEN-325, TEN-326, TEN-327)
* Refactor standards content to include a Level lookup. Current method of determining level is inefficient (TEN-317, TEN-318, TEN-319)

### Tenon Tests

* A number of new tests for valid ARIA usage
* More robust tests for JavaScript events bound accessibility.
* More mobile-web accessibility tests
* New tests for CSS-generated content and sprites on controls
* More robust & accurate tests of tables

### Tenon API

* Return min & max priority in result response (TAPI-115)
* Return min & max certainty in result response (TAPI-114)  
  
## Beyond 1.1 or at risk for backlogging

* Ability to supply an `xPath` parameter as part of request, which will limit testing only to a specific portion of the page (TAPI-2)
* Ability to log associated nodes. Use case: A test exists that will find non-unique form fields. This would allow us to also provide xPath/ line & column info for other fields with the same IDs (TEN-286)
* Add relative priority to the Best Practices Quality Report. This is an internal use report that allows us to prioritize our own efforts and improving Best Practices content (TEN-52)
* Added RESTful-ness. Our medium-to-long term goal is to move toward a more RESTful API offering more full-featured CRUD access to manage users, systems, reports, and issues.