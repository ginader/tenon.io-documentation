#Tenon.io Quick Start

## Quick note:

Tenon is currently in private beta. This documentation discusses registering to get an API key, which is currently inaccurate.
While in private beta, you can request an API key by doing the following:

* Go to [http://www.tenon.io](http://www.tenon.io) to get on the mailing list for new beta announcements
* AND email karl@tenon.io directly to request the key.

## Intro 

To see Tenon in action and to verify it doesn't suck: take a quick trip to:
[http://tenon.io/#testnow](http://tenon.io/#testnow)

Now, let's dive in:

Reading endless documentation is no fun. Let's get started:

1. You need an API key. If you don't have one,  go to http://www.tenon.io/register.php (*See note above*)
2. Some things to know in advance:
   1. The API URL is http://www.tenon.io/api/
   2. You'll be sending a POST request to that URL. GET requests get no response.
   3. The only required parameter is 'key'. Put your API key here
   4. You must also supply either an 'url' parameter or a 'src' parameter. Their names are self-explanatory: the 'url' is the address of a publicly reachable URL. The 'src' is the full document source for a page you want tested.
   5. All other parameters are optional so we aren't talking about them here.
   
## Example requests

Below we show a handful of examples for submitting a request to the API. All examples below show submitting a request to test an URL.  Note: none of these are "production-ready" code. You'll need to customize them to make them your own.

### cURL
The single easiest way to test the API is from command line, using cURL to POST your request:

```
$ curl -X POST -H Content-Type:application/x-www-form-urlencoded -H Cache-Control:no-cache -d 'url=URL-TO-TEST&key=PUT-YOUR-KEY-HERE' http://www.tenon.io/api/'
```

Naturally, in the above, you'd replace `URL-TO-TEST` with the URL you want to test and you'd replace `PUT-YOUR-KEY-HERE` with your API key.

As you'll see by running thos command, the response is JSON-formatted and therefore it is not very useful to use Tenon strictly from the command line, but you get the point. 

### PHP & cURL

A more useful approach would be to POST the request using PHP and cURL. The below example shows getting the JSON response as a variable named `$result`.


```
$opts['key'] = 'YOUR API KEY GOES HERE';
$opts['url'] = 'YOUR URL TO TEST GOES HERE';

// open connection
$ch = curl_init();

// set our curl options
curl_setopt($ch, CURLOPT_URL, 'http://www.tenon.io/api/');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_FAILONERROR, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $opts);

//execute post and get results
$result = curl_exec($ch);

//close connection
curl_close($ch);

//this convers the JSON API response to a PHP array
$result = json_decode($result, true);

//now do something useful with the array of data

```

### Python 2 & urllib

```
import urllib
params = urllib.urlencode({'url': 'PUT-YOUR-URl-HERE', 'key': 'PUT-YOUR-KEY-HERE'})
f = urllib.urlopen("http://www.tenon.io/api/", params)
print f.read()
```

### Python 3 & urllib

```
import urllib.request
import urllib.parse
data = urllib.parse.urlencode({'url': 'PUT-YOUR-URL-HERE', 'key': 'PUT-YOUR-KEY-HERE'})
data = data.encode('utf-8')
request = urllib.request.Request("http://www.tenon.io/api/")
# adding charset parameter to the Content-Type header.
request.add_header("Content-Type","application/x-www-form-urlencoded;charset=utf-8")
f = urllib.request.urlopen(request, data)
print(f.read().decode('utf-8'))
```


### Node.js

```
var querystring = require('querystring');

var data = querystring.stringify({
      url: PUT-YOUR-URL-HERE,
      key: PUT-YOUR-KEY-HERE
    });

var options = {
    host: 'www.tenon.io',
    port: 80,
    path: '/api/',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(data)
    }
};

var req = http.request(options, function(res) {
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
        console.log("body: " + chunk);
    });
});

req.write(data);
req.end();
```
Naturally you'll want to do something more useful with this example than simply logging it to console.

## Dealing with the response

The methods above will return a JSON response which you'll then need to do something useful with. Read 'Overview of the Tenon API Response' elsewhere in these docs to get an overview of the exact response format.

## Additional parameters

There are a variety of additional parameters that can be submitted with your request. Read 'Understanding API Request Parameters' to see what they are.
# Tenon.io API Developer Documentation

The following information describes the Tenon.io API, including all actions that can be taken using the API, all parameters (required and optional), and all response codes that are returned.

## Introduction
Tenon.io is a web-based application and API that facilitates the testing of web-based products and services for compliance with major standards for Accessibility.

## Abuse
Every single request against this API is tracked by our system.  Any abusive behavior *will*  cause an immediate and irreversible revocation of your privileges to use this API. For some types of abuse this will happen at the very first sign of abuse, with no warning whatsoever.

## Obtaining Access to the API

*Please see note in Tenon Quickstart*

To obtain access to the API you must register at [http://www.tenon.io/register.php](http://www.tenon.io/register.php).  Upon successfully registering for the API, you'll receive an email in which you will find an URL to confirm your registration. After clicking on the confirmation link, you will be set up with an API key.

## Using the API
Using the API is dead simple: send a POST request to the API with your API key and an URL and we shoot back results.  There are also a number of additional parameters you can use to fine-tune what types of errors are returned. 

### API Request/ Response overview
At a high level, here's how Tenon works:

1. Your client application submits a request against the API
2. Tenon receives the request and validates it
3. If the request passes validation, Tenon creates a new record in the database, associated with your account
4. Tenon runs its tests against the URL or document source
5. Tenon returns a JSON response and also stores the response in the database.

### Submitting Requests

* The URL to submit your requests is http://tenon.io/api
* All requests must be POST requests.  GET requests will be rejected entirely without a meaningful response.
* All requests must contain API key and either document source as a string or an URL to a publicly available URI. The API validates these parameters and, if they're invalid or out of bounds, processing will stop and you will get an error response.
* A full list of request parameters can be found in "Understanding API Request Parameters"

### Responses
Response will be returned to you in the form of JSON. JSON is the only format we use. The JSON string will be formatted as follows for all responses. For error responses the resultSet node will not exist. For a full description of the API response, see "Overview of the Tenon API Response".

## Notes For Your Success
  * All tests contain what we calla a 'certainty' score. This is described in both "Understanding API Request Parameters" and "Overview of the Tenon API Response".  Items with a low certainty score are likely to become “white noise” in the report output. On a practical level you may want to accept only items with a high certainty score.  Be careful that you may be filtering valid results.
  * We do not recommend that you cache or store the error information (title, description, etc.) because it’s highly likely that the tests, titles, and descriptions will vary and evolve over time.  As we develop and improve the testing and reporting this information is also likely to improve.  Storing it will mean you don’t have the latest and best information.

### Testing Document Source vs. URL
The Tenon.io web service is JavaScript-aware. This means that it uses a headless browser to do its testing. This allows it to be highly accurate when testing interfaces that use JavaScript to generate or modify interface elements.  To get the most out of the Tenon.io web service, you should submit an URL to a publicly accessible location. This will ensure that Tenon.io can perform the most accurate "real world" testing. This may be difficult for testing pages in a development environment which is behind a firewall, VPN, or other environment that requires authentication. The Tenon.io service is capable of testing document source. It will take the string that you supply in the 'src' parameter of the request, turn it into a web page, and test that. If you submit document source, it would be best to ensure that source paths to JavaScript and CSS files be accessible and coded in a way that allows the API to get to it. One way of ensuring this is to use CDN hosted versions of your files. Alternately, you can send over the final rendered version of the page as document source. The more closely your source reflects the final rendered DOM of the page, the better. 

Remember, a publicly accessible URL is always best because it allows us to test the real page as it would be experienced by the user.

## Getting the most out of the API
  * **Test early, test often.** Accessibility issues are best if they're avoided entirely, so test your code as you develop, not after it is already deployed. Post deployment remediation is much more expensive than avoiding the problem in the first place. This is the entire reason why we've created an API rather than a fully-featured standalone system.
  * **Tweak your settings.**  Experiment with optional parameters like 'certainty', 'priority', and 'level' to make sure you are getting useful and accurate results.  Its our goal to make sure we're only providing useful and accurate results but sometimes context matters. If you find that you're getting a lot of false positives, let us know.
  * **Speed up your site.** In our own testing, almost all instances where we throw a timeout error it is because the URL being tested is slow.  If you get a timeout error, test your site with YSlow. If you get anything other than a 'D' or 'F' from YSlow, email us.

#Understanding Tenon API Request Parameters

This document discusses all of the various request parameters available to Tenon API users. The number and kinds of request parameters we provide allows Tenon to work for you in a way that matches your specific goals and needs.

There are three types of parameters described below:
1. Required parameters
2. Optional parameters
3. Paid-only parameters

##Required Parameters
There are only two required parameters: 'key' and *either* 'url' or 'src'.

###key

The `key` parameter is the API key you received upon registering. The key is required for every API request you send. Missing or invalid keys will result in error response. 

###url *or* src

Either the `url` or `src` are required for testing.  The URL must be a fully qualified URL that the API has access to. If any `GET` parameters are required in order to access the URL, you must supply them as part of the URL.  If using `src` it must be a string of document source only (HTML, CSS, and JavaScript).

Note: Supplying both `url` and `src`, beyond being plain silly, will result in only the URL being tested. 

When supplying `src`, this can be anything ranging from the source of a complete document or only a fragment of a document (mark this case using the `fragment` parameter, described below). The `src` is preferable when testing something that is otherwise not available via URL.  

If using `src`, ensure that the path to all document assets is set to a fully qualified path, otherwise those assets (and their impact on the UI) will not be accurately tested. For instance, if you're testing a document fragment that consists of a JavaScript-driven widget, you'll need to also do one of the following:

* include either the JavaScript and CSS source in the document itself
* include the fully qualified path of the relevant assets in the relevant `<script>` and `<link>` tags of the source string 
* or, send over the rendered document source in its state after the JavaScript 
 
*Remember: the important part of testing is to test that which will be experienced by the user which means, if testing document source, you should send over a "rendered" version of the source*

Note: If you plan to send over document source, you may want to explore utilizing a Grunt plugin like [Grunt HTML Smoosher](https://www.npmjs.org/package/grunt-html-smoosher) to help prepare the source.

## Optional Parameters

*All* of the below parameters are optional for all users.

Some of these parameters may not be available to you depending upon your plan. For specific details on what your plan allows, review your plan details.

Providing any value outside of the accepted list of values will result in failure

###certainty

Sometimes accessibility tools will return results that the tool isn't completely sure is a real issue. The `certainty` parameter allows you to filter out these "uncertain" results by choosing a minimum acceptable certainty score. 

* Valid values: must be one of '0','20','40','60','80','100'
* The default value is '0', meaning that the minimum certainty score for issues returned is '0'

Note: You probably don't want to only choose '100' as this will, in practice, probably be too conservative. We try to create tests that return real results and so very low certainty scores should be few and far between. As a general guide, if integrating Tenon into an automated build or pre-commit hook, use 80 and above. For a QA scenario, choose the lowest value you can tolerate as an organization.

###fragment

The `fragment` parameter only works when submitting content to the API via the `src` attribute. Using `fragment` when testing an url will be ignored.  This parameter allows you to identify the source string as only a fragment of a page.  This will essentially filter out the reporting of document-level issues and return results relevant only to the source fragment you submit.

* Valid values: must be one of '0','1' representing false and true.
* The default value is '0'

Note: do not set `fragment` to '1' if you're sending over a complete document. You *will* get weird results.


###importance

The importance parameter allows you to indicate a relative importance of this specific document against others you may be testing.  For instance, the site's home page or contact page may be the most important in your site and thus be rated as "High" (indicated by '3') importance. This allows issues on important pages to "bubble up" compared to the issues on other pages. Therefore, this is only relevant when testing multiple pages.

* Valid values: must be one of '0','1','2','3' representing "None", "Low", "Medium", and "High".
* The default value is '0', meaning that the minimum importance used in calculating final relative priority for issues returned is '0'

This parameter is used when calculating final issue priority in the results.  The `importance` parameter is only really relevant or useful when compiling a report set consisting of multiple documents. By doing so, the issues on the *more* important document will be correspondingly more important than those on other documents that were tested.

###level

The level parameter indicates the "lowest" WCAG level to test against.

* Valid values: must be one of 'A','AA','AAA'.  Chosing "AA", for instance, will test both "AA" and "A" success criteria. 
* Default value is 'AAA', meaning that all tests for AAA, AA, and A will be run. 

Note: Keep in mind that WCAG Level does not actually correlate to Priority when determining what to test for.  For more information on our philosophy regarding prioritization, see blog post by Karl Groves: [Understanding WCAG Level](http://http://www.karlgroves.com/2013/05/20/understanding-wcag-level/)

In practice, very few of the Level AAA Success Criteria are testable using automated means and therefore you'll see few tests against these anyway. Despite this, we recommend selecting "AAA" for the `level` parameter and being more stringent in your `certainty` setting, because it is likely that doing so will have a greater impact against False Positives than filtering by Level.

###priority

Each Best Practice in the system has a number of factors that allow us to determine their relative priority. In your result set, each issue reported will be given a calculated priority score which you can then use to filter or order your results.

* Valid values: must be one of '0','20','40','60','80','100'
* Default value will be '0'. As a consequence all issues, regardless of Priority, will be reported.

Note: in practice, it is unlikely that an issue returned will have 100% priority. Like `certainty`, you should supply the lowest priority you can, especially when first starting out. Setting this too high may result in no errors returned.   

###ref

The `ref` parameter indicates whether or not you wish to be given a link with each issue which includes reference information for the violated Best Practice.  This reference information can be immensely useful for your team if your organization is new to accessibility, as the URL provided will supply extensive details on the issue as well as remediation guidance.

* Valid values: must be one of '0','1' representing 'false' and 'true'.
* Default value is '1', meaning that each issue reported will also have an associated URL at which you can find reference material.

Note: you can greatly diminish the size of the JSON response by passing '0' for this parameter.


###reportID

The `reportID` parameter is a string of text you can supply to identify the request as part of a specific report. This allows the requests to be grouped together as though they were part of the same test effort.

* Valid values: can be any arbitrary alphanumeric string up to 255 characters in length.

Note: it is your responsibility to ensure accuracy and validity of the supplied string. We will accept any (safe) arbitrary string for this parameter. If no value is supplied, we will create one.

###store

The `store` parameter indicates whether or not Tenon should store your results. You can then come back later and view your results in our Web-based system.

* Valid values: must be one of '0','1' representing 'false' and 'true'.
* Default value is '0', meaning that we will not store the results.

Note: The availability of this parameter and length of time we will store the results depends upon your plan.


###systemID

The `systemID` parameter is a string of text you can supply to identify the tested document as part of a specific system. This can (and should) be used in conjunction with the reportID parameter so that you can identify your request as being part of a test effort (a report) on a specific system.  This is especially useful if you are developing or testing multiple projects.

* Valid values: can be any arbitrary alphanumeric string up to 255 characters in length.

Note: it is your responsibility to ensure accuracy and validity of the supplied string. We will accept any (safe) arbitrary string for this parameter. If no value is supplied, we will create one.


###uaString

The `uaString` parameter allows you to supply any arbitrary string of text which will be sent as the User-Agent string when we request the URL you supply. This is particularly useful if your site does any user-agent sniffing.

* Valid values: can be any arbitrary string up to 255 characters in length.

Note: it is your responsibility to ensure accuracy and validity of the supplied string. We will accept any (safe) arbitrary string for this parameter. If no value is supplied, we will create one.


###viewPortHeight

The `viewPortHeight` parameter sets the height, in pixels, of the viewport for our headless browser. 

* Valid values: must be a string consisting only of numeric characters, no more than four characters in length.  In other words, it can be any positive number between 0 and 9999.

Note: If this parameter it is not supplied at time of request it will be set to '768'. *If you set one viewport parameter you must set both*


###viewPortWidth
The `viewPortWidth` parameter sets the width, in pixels, of the viewport for our headless browser. This parameter is particularly useful when testing the accessibility of your responsive design. 

* Valid values: must be a string consisting only of numeric characters, no more than four characters in length.  In other words, it can be any positive number between 0 and 9999. 

Note: If this parameter it is not supplied at time of request it will be set to '1024'. *If you set one viewport parameter you must set both*
# Overview of the Tenon API Response

Upon submission of your request against the Tenon API, we will return a JSON formatted response for your client application to consume. This response will include a large amount of detail useful to you in determining the quality of your request and will detail the test results generated based on that request. 

This document explains every possible node in the JSON response.

* **status:** HTTP status code for your response. Values will be 200, 401, 403, 404, 500, or 522
* **message:** A specific text message indicating the nature of the response
* **documentSize:** size, in bytes of the document you gave us to test
* **responseExecTime:** How long, in seconds, it took for us to test your document
* **responseTime:** Time of your request, localized to GMT
* **sourceHash:** an MD5 hash of your document source, useful in uniquely identifying your tested documents
* **request:**  This node is a complete mirror of the request you sent to us. Any values echoed here which you did not supply in your request will be set to their defaults, as discussed in "Understanding Tenon API Request Parameters"
  * certainty
  * docID
  * importance
  * key
  * level
  * priority
  * priorityWeightissueLocation
  * ref
  * reportID
  * responseID
  * systemID
  * uaString
  * url
  * viewport
    * height
    * width
  * fragment
  * store
* **clientScriptErrors:** this node will generate an array of errors generated by the script(s) in your tested document. These errors may impact our ability to effectively test your document, so we list them here so you can fix them and retest. Each error contains the following information:
  * message: The exception message
  * stacktrace: This is a full stack trace of the error
    * file: the file where the error exists
    * line:  the line where the error exists
    * function: the function where the error exists
* **globalStats: **The global stats node exists to allow you to compare your document against others that have been tested. Global Stats are a calculation of *all* tested documents. These stats relate only to "density" - that is, per KB of code, what percentage contain accessibility issues.
  * allDensity: Density, as a percentage, of both errors and warnings
  * errorDensity: Density, as a percentage, of errors only
  * warningDensity: Density, as a percentage, of warnings only
* **resultSummary:** The resultSummary node provides a high level overview of our test results.
  * density: As mentioned above in 'globalStats' the density node is a calculation of the issue density in *your* tested document
    * allDensity
    * errorDensity
    * warningDensity
  * issues: This is a count of the issues in your document
    * totalErrors
    * totalIssues
    * total warnings
  * issuesByLevel: This is a count of the issues in your document, separated out by WCAG Level.
    * A
      * count
      * pct
    * AA
      * count
      * pct
    * AAA
      * count
      * pct
* **resultSet:** The resultSet will contain an array for each issue discovered during testing. Each issue will be presented with the following keys. Each of these are described in full in the document titled "Understanding Issue Reports in Tenon.io API Response"
  * bpID
  * certainty
  * errorDescription
  * errorSnippet
  * errorTitle
  * position
    * line
    * column
  * ref
  * resultTitle
  * signature
  * standards
  * tID
  * xpath 

### JSON Response Example

Here's an example of the JSON response

```
{
    response: {
        documentSize: 4709,
        message: "success",
        request: {
            certainty: "0",
            docID: "908a891f-82ab-4b40-bebd-fec29238c14b",
            importance: "0",
            key: "null",
            level: "AAA",
            priority: "0",
            priorityWeightissueLocation: "1",
            ref: "0",
            reportID: "4d978d9c-005c-47a1-bb8b-0be11f7e59e2",
            responseID: "null",
            systemID: "null",
            uaString: "Mozilla/5.0 (Macintosh; Intel Mac OS X) AppleWebKit/534.34 (KHTML, like Gecko) PhantomJS/1.9.2 Safari/534.34",
            url: "http://www.example.com/foo.html",
            viewport: {
                height: 300,
                width: 400
            },
            fragment: "0",
            store: "1",
        responseExecTime: "0.12",
        responseTime: "2014-04-18T15:36:23.065Z",
        clientScriptErrors: [
                {
                    message: "ReferenceError: Can't find variable: e",
                    stacktrace: [
                        {
                            file: "foo.html",
                            line: 123,
                            function: "bar"
                        },
                        {
                            file: "foo.html",
                            line: 129,
                            function: "bat"
                        },
                        {
                            file: "foo.html",
                            line: 133,
                            function: ""
                        }
                    ]
                }
            ]
        },
        globalStats: {
            allDensity: "0",
            errorDensity: "0",
            warningDensity: "0"
        },
        resultSet: [
            {
                bpID: "118",
                certainty: "100",
                errorDescription: "Language of the document has not been set",
                errorSnippet: "&lt;html class="no-js"&gt;&lt;!--&lt;![endif]--&gt;&lt;head&gt; &lt;meta charset="utf-8"&gt; &lt;title&gt;&lt;/title&gt; &lt;style type="text/css"&gt; body { width: 1600px; margin: 0; padd",
                errorTitle: "Language not set",
                position: {
                    column: 24,
                    line: 3
                },
                priority: 73,
                ref: "null",
                resultTitle: "Identify the document language",
                signature: "44a94237c000e850648a666c8f85d30d",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 3.1.1 Language of Page"
                ],
                tID: "3",
                xpath: "/html"
            },
            {
                bpID: "1",
                certainty: "100",
                errorDescription: "All images must have an alt attribute",
                errorSnippet: "&lt;img src="http: //upload.wikimedia.org/wikipedia/commons/5/52/Spacer.gif"&gt;",
                errorTitle: "Image missing alt attribute",
                position: {
                    column: 6,
                    line: 42
                },
                priority: 82,
                ref: "null",
                resultTitle: "Provide alt attributes for all IMG elements",
                signature: "b22e6ea3a2b80f6d14b313ba9be274a0",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 1.1.1 Non-text Content"
                ],
                tID: "9",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[4]/img[1]"
            },
            {
                bpID: "1",
                certainty: "100",
                errorDescription: "All images must have an alt attribute",
                errorSnippet: "&lt;img src="http: //upload.wikimedia.org/wikipedia/commons/5/52/Spacer.gif" title="Aspacerimage"&gt;",
                errorTitle: "Image missing alt attribute",
                position: {
                    column: 6,
                    line: 50
                },
                priority: 82,
                ref: "null",
                resultTitle: "Provide alt attributes for all IMG elements",
                signature: "614c5305042885252f224cf39433a512",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 1.1.1 Non-text Content"
                ],
                tID: "9",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[6]/img[1]"
            },
            {
                bpID: "8",
                certainty: "100",
                errorDescription: "Code was found which uses deprecated methods to define colors. This cannot be overridden or disabled by CSS.",
                errorSnippet: "&lt;td bgcolor="eeeeee"&gt; &lt;div class="ex"&gt; &lt;!--marquee element--&gt; &lt;marquee&gt; The marquee tag is a non-standard HTML element which causes text to scroll up, down, left or right automatically. The tag was first introd",
                errorTitle: "Document uses deprecated color definition",
                position: {
                    column: 5,
                    line: 21
                },
                priority: 91,
                ref: "null",
                resultTitle: "Ensure content and components are readable with CSS turned off",
                signature: "f7364936aaca5260d2096f1a9cb7f0e3",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 1.3.2 Meaningful Sequence"
                ],
                tID: "14",
                xpath: "/html/body/table[1]/tr[1]/td[1]"
            },
            {
                bpID: "8",
                certainty: "100",
                errorDescription: "Code was found which uses deprecated methods to define colors. This cannot be overridden or disabled by CSS.",
                errorSnippet: "&lt;font size="2" color="purple"&gt;The &amp;lt;font&amp;gt; tag provides no real functionality by itself, but with the help of a few attributes, this tag is used to change the style, size, and color of HTML text elements. The size, color, and face attri",
                errorTitle: "Document uses deprecated color definition",
                position: {
                    column: 7,
                    line: 112
                },
                priority: 91,
                ref: "null",
                resultTitle: "Ensure content and components are readable with CSS turned off",
                signature: "2d3b0b146d4d4a5305a2308bee4486da",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 1.3.2 Meaningful Sequence"
                ],
                tID: "14",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[16]/p[1]/font[1]"
            },
            {
                bpID: "127",
                certainty: "80",
                errorDescription: "Table found that was direct descendent of body. This typically means tables are used for layout.",
                errorSnippet: "&lt;table id="tablesForLayoutAreAwesome"&gt; &lt;tbody&gt;&lt;tr&gt; &lt;td bgcolor="eeeeee"&gt; &lt;div class="ex"&gt; &lt;!--marquee element--&gt; &lt;marquee&gt; The marquee tag is a non-standard HTML element which causes te",
                errorTitle: "Avoid using tables for layout",
                position: {
                    column: 3,
                    line: 19
                },
                priority: 73,
                ref: "null",
                resultTitle: "Avoid using header cells and other semantic table content in layout tables",
                signature: "21a1b1e32d8dd65e21dd1e6c9375314a",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 1.3.1 Info and Relationships",
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 1.3.2 Meaningful Sequence"
                ],
                tID: "15",
                xpath: "/html/body/table[1]"
            },
            {
                bpID: "18",
                certainty: "100",
                errorDescription: "Do not use deprecated elements. They cannot be overridden by the user",
                errorSnippet: "&lt;font size="2" color="purple"&gt;The &amp;lt;font&amp;gt; tag provides no real functionality by itself, but with the help of a few attributes, this tag is used to change the style, size, and color of HTML text elements. The size, color, and face attri",
                errorTitle: "Deprecated elements found",
                position: {
                    column: 7,
                    line: 112
                },
                priority: 73,
                ref: "null",
                resultTitle: "Avoid deprecated markup",
                signature: "65d057b30f1c6c9e6bf0a23a77507b1e",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 4.1.1 Parsing"
                ],
                tID: "16",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[16]/p[1]/font[1]"
            },
            {
                bpID: "148",
                certainty: "100",
                errorDescription: "This fieldset does not have a legend",
                errorSnippet: "&lt;fieldset&gt; &lt;p&gt; &lt;strong&gt;Which German model is hotter?&lt;/strong&gt; &lt;/p&gt; &lt;div&gt; &lt;input type="radio" name="model" value="Jordan_carver"&gt; Jordan Carver &lt;/div&gt; &lt;d",
                errorTitle: "Fieldsets must have legend",
                position: {
                    column: 6,
                    line: 73
                },
                priority: 73,
                ref: "null",
                resultTitle: "Provide fieldsets for all legend elements",
                signature: "b8323cbc49912f486178d8c59907c5c9",
                standards: [
                ],
                tID: "17",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[11]/fieldset[1]"
            },
            {
                bpID: "135",
                certainty: "100",
                errorDescription: "The BLINK and MARQUEE element animate content in a way that cannot be overridden or disabled by the user.",
                errorSnippet: "&lt;marquee&gt; The marquee tag is a non-standard HTML element which causes text to scroll up, down, left or right automatically. The tag was first introduced in early versions of Microsoft's Internet Explorer, and was compared to Netscape's blink ",
                errorTitle: "BLINK or MARQUEE element found",
                position: {
                    column: 6,
                    line: 24
                },
                priority: 91,
                ref: "null",
                resultTitle: "Avoid use of the blink and marquee elements",
                signature: "97c1fe9b2a52d56d0c61edf03b3b87d9",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 2.3.1 Three Flashes or Below Threshold",
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AAA: 2.3.2 Three Flashes"
                ],
                tID: "19",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[1]/marquee[1]"
            },
            {
                bpID: "135",
                certainty: "100",
                errorDescription: "The BLINK and MARQUEE element animate content in a way that cannot be overridden or disabled by the user.",
                errorSnippet: "&lt;blink&gt; The blink element is a non-standard presentational HTML element that indicates to a user agent (generally a web browser) that the page author intends the content of the element to blink (that is, alternate between being visible and in",
                errorTitle: "BLINK or MARQUEE element found",
                position: {
                    column: 6,
                    line: 30
                },
                priority: 91,
                ref: "null",
                resultTitle: "Avoid use of the blink and marquee elements",
                signature: "d9e27e32487bdcbd606ba97f778f0509",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 2.3.1 Three Flashes or Below Threshold",
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AAA: 2.3.2 Three Flashes"
                ],
                tID: "19",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[2]/blink[1]"
            },
            {
                bpID: "15",
                certainty: "100",
                errorDescription: "Do not use elements FONT or CENTER in your code. Use CSS instead",
                errorSnippet: "&lt;font size="2" color="purple"&gt;The &amp;lt;font&amp;gt; tag provides no real functionality by itself, but with the help of a few attributes, this tag is used to change the style, size, and color of HTML text elements. The size, color, and face attri",
                errorTitle: "Deprecated typography coding found",
                position: {
                    column: 7,
                    line: 112
                },
                priority: 82,
                ref: "null",
                resultTitle: "Use CSS to control visual presentation of text",
                signature: "aadcd2310d9454acc5c11f42f525ec13",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AA: 1.4.5 Images of Text"
                ],
                tID: "26",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[16]/p[1]/font[1]"
            },
            {
                bpID: "3",
                certainty: "100",
                errorDescription: "The size of the layout may cause horizontal scroll for users on lower resolutions, such as users with low vision",
                errorSnippet: "&lt;body style=""&gt; &lt;table id="tablesForLayoutAreAwesome"&gt; &lt;tbody&gt;&lt;tr&gt; &lt;td bgcolor="eeeeee"&gt; &lt;div class="ex"&gt; &lt;!--marquee element--&gt; &lt;marquee&gt; The marquee tag is a non-standard HTML",
                errorTitle: "Avoid layouts and sizing which cause horizontal scrolling",
                position: {
                    column: 2,
                    line: 18
                },
                priority: 64,
                ref: "null",
                resultTitle: "Avoid layouts and sizing which cause horizontal scrolling",
                signature: "9ab7eae3fe7deec3f59a138870773671",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AAA: 1.4.8 Visual Presentation"
                ],
                tID: "28",
                xpath: "/html/body"
            },
            {
                bpID: "131",
                certainty: "60",
                errorDescription: "",
                errorSnippet: "&lt;table id="tablesForLayoutAreAwesome"&gt; &lt;tbody&gt;&lt;tr&gt; &lt;td bgcolor="eeeeee"&gt; &lt;div class="ex"&gt; &lt;!--marquee element--&gt; &lt;marquee&gt; The marquee tag is a non-standard HTML element which causes te",
                errorTitle: "Headers not identified for table",
                position: {
                    column: 3,
                    line: 19
                },
                priority: 82,
                ref: "null",
                resultTitle: "Provide headers for all data tables",
                signature: "aa62a39e4d388d9b358af0702d5fef4a",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 1.3.1 Info and Relationships"
                ],
                tID: "34",
                xpath: "/html/body/table[1]"
            },
            {
                bpID: "39",
                certainty: "80",
                errorDescription: "",
                errorSnippet: "&lt;label for="whee"&gt;This submits onblur&lt;/label&gt;",
                errorTitle: "Non-unique field label found",
                position: {
                    column: 6,
                    line: 58
                },
                priority: 82,
                ref: "null",
                resultTitle: "Ensure field labels are unique",
                signature: "0ade709e26ef14e35ee4d83c90a032f8",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AA: 2.4.6 Headings and Labels"
                ],
                tID: "44",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[8]/label[1]"
            },
            {
                bpID: "39",
                certainty: "80",
                errorDescription: "",
                errorSnippet: "&lt;label for="radio"&gt;Onchange radio&lt;/label&gt;",
                errorTitle: "Non-unique field label found",
                position: {
                    column: 6,
                    line: 64
                },
                priority: 82,
                ref: "null",
                resultTitle: "Ensure field labels are unique",
                signature: "ec15a862587aceacd5abd39de3e349eb",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AA: 2.4.6 Headings and Labels"
                ],
                tID: "44",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[9]/label[1]"
            },
            {
                bpID: "39",
                certainty: "80",
                errorDescription: "",
                errorSnippet: "&lt;label for="checkbox"&gt;Onclick checkbox&lt;/label&gt;",
                errorTitle: "Non-unique field label found",
                position: {
                    column: 6,
                    line: 69
                },
                priority: 82,
                ref: "null",
                resultTitle: "Ensure field labels are unique",
                signature: "d8dd1a548b072a31ced2ce345d79e7f1",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AA: 2.4.6 Headings and Labels"
                ],
                tID: "44",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[10]/label[1]"
            },
            {
                bpID: "68",
                certainty: "80",
                errorDescription: "",
                errorSnippet: "&lt;p tabindex="5"&gt; If you use tabindex greater than zero you're gonna have a bad time &lt;/p&gt;",
                errorTitle: "Do not apply tabindex to elements that are not actionable and don't have events bound to them",
                position: {
                    column: 6,
                    line: 36
                },
                priority: 64,
                ref: "null",
                resultTitle: "Avoid applying focus to items which are not actionable",
                signature: "776bbfbe58e4b4a20e958537ef76be1f",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 2.1.1 Keyboard",
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AAA: 2.1.3 Keyboard (No Exception)"
                ],
                tID: "55",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[3]/p[1]"
            },
            {
                bpID: "177",
                certainty: "100",
                errorDescription: "Don't use the accesskey attribute",
                errorSnippet: "&lt;a href="http: //www.example.com" accesskey="H"&gt;Home&lt;/a&gt;",
                errorTitle: "Avoid Accesskey",
                position: {
                    column: 7,
                    line: 100
                },
                priority: 73,
                ref: "null",
                resultTitle: "Avoid the use of ACCESSKEY attribute",
                signature: "f6a6d3c4096ba47e2270893a6ac23e96",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 2.1.1 Keyboard",
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 2.1.2 No Keyboard Trap"
                ],
                tID: "59",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[14]/p[1]/a[1]"
            },
            {
                bpID: "5",
                certainty: "100",
                errorDescription: "Remove all FONT elements. Use CSS for typography",
                errorSnippet: "&lt;font size="2" color="purple"&gt;The &amp;lt;font&amp;gt; tag provides no real functionality by itself, but with the help of a few attributes, this tag is used to change the style, size, and color of HTML text elements. The size, color, and face attri",
                errorTitle: "Font element found",
                position: {
                    column: 7,
                    line: 112
                },
                priority: 73,
                ref: "null",
                resultTitle: "Avoid using deprecated methods (font element) to define font sizes",
                signature: "5b92d7b216ff6a2d44e4cedf5e75d97c",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level AA: 1.4.4 Resize text"
                ],
                tID: "63",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[16]/p[1]/font[1]"
            },
            {
                bpID: "25",
                certainty: "80",
                errorDescription: "Remove all tabindex values higher than zero",
                errorSnippet: "&lt;p tabindex="5"&gt; If you use tabindex greater than zero you're gonna have a bad time &lt;/p&gt;",
                errorTitle: "Tabindex greater than 0",
                position: {
                    column: 6,
                    line: 36
                },
                priority: 82,
                ref: "null",
                resultTitle: "Ensure tab order reflects expected interaction order",
                signature: "2c3eed0c6194c12f60adfbb8dc1f792a",
                standards: [
                    "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 2.4.3 Focus Order"
                ],
                tID: "66",
                xpath: "/html/body/table[1]/tr[1]/td[1]/div[3]/p[1]"
            }
        ],
        resultSummary: {
            density: {
                allDensity: 42,
                errorDensity: 40,
                warningDensity: 2
            },
            issues: {
                totalErrors: 19,
                totalIssues: 20,
                totalWarnings: 1
            },
            issuesByLevel: {
                A: {
                    count: 0,
                    pct: 0
                },
                AA: {
                    count: 0,
                    pct: 0
                },
                AAA: {
                    count: 0,
                    pct: 0
                }
            }
        },
        sourceHash: "4abd7e9151468a463c5c63f468e5fe45",
        status: "200"
    }
}
```

#Understanding Response Codes from Tenon API

Tenon API uses HTTP Status codes to indicate whether your request was a success or failure. We also provide some additional information useful in determining the nature of the failure.  The following example illustrates the relevant portion of our JSON response at which you'll locate this information:

* **status:** the HTTP status code
* **message:** The associated message for the code, per [RFC 2616](http://tools.ietf.org/html/rfc2616)
* **code:** a token string showing what the response was
* **moreInfo:** an URL to a more detailed explanation of the code

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "invalid_param",
        "moreInfo": "https://www.tenon.io/docs/invalid_param";
    }
}
```

##Success Codes

### 'success'

The 'success' codes is easy to understand. This code means all went well with the request. All required parameters were set, the API key was valid, the supplied parameters were valid, the test document was reachable, and we were able to successfully perform our testing and return a response.

####Example:

```
{
    "response": {
        "status": "200",
        "message": "OK",
        "code": "success",
        "moreInfo": "https://www.tenon.io/docs/success";
    }
}
```

## Failure Codes

### Service Issues

The following failure codes represent that something went wrong on our end. Performance of our service is highly important to us. If you experience repeated performance issues please contact us so that we may investigate what is happening.

#### 'exception'

This means an exception happened on our end. Our system is currently running at 94% success rate, meaning 94% of requests run without issue. If you get a 500 error, chances are, you can re-run your request without issue, so we recommend that in your final implementation, if you get a 500 error, retry once or twice. Re-running more than twice is a bad idea because, in our own use of the system we never see more than two 500s in a row unless something is seriously wrong. 

#####Example

```
{
    "response": {
        "status": "500",
        "message": "Internal Server Error",
        "code": "exception",
        "moreInfo": "https://www.tenon.io/docs/exception";
    }
}
```

#### 'request_timeout'

This means that the overall process took longer than we allow.  The average successful response time is between 4 - 7 seconds for most requests - that's from the time you submit your request to the time we return a response.  We actually wait much longer for the process to complete, so if you get this response, something went wrong. However, ** the issue may be on your side ** Request timeouts tend to happen for one of three reasons:

* Something's wrong with your JavaScript: either your JavaScript has uncaught exceptions or your page never fires the onload event.  In the latter case, we attempt to overcome this on our end but sometimes it just doesn't workout and we just kick it back. 
* Your page includes third party content or features that never finish loading.  Related to the above, sometimes timeouts are caused by third party stuff that never finishes loading
* Your page's performance is too slow.
* Your page's accessibility issues are so numerous that testing & processing takes so long that the process times out. This is actually a severe edge case, as we've seen pages with as many as 1800 errors that still responded, so this is far less likely than the other two cases, but still probable.

The issue in your case may be one of the above or a combination of the above. Regardless, we recommend investigating what the issue may be. This is especially true if you get multiple 522 responses. While a single 522 response here and there likely resides on our end, multiple 522 responses for multiple pages on the same site is likely to mean something to do with your code. In that case, you should investigate page performance, looking specifically for uncaught exceptions in your JavaScript.

#####Example

```
{
    "response": {
        "status": "522",
        "message": "Connection Time Out",
        "code": "request_timeout",
        "moreInfo": "https://www.tenon.io/docs/request_timeout";
    }
}
```

### Request Issues

400 & 401 status responses are likely to be the most common failure responses you'll find and are due to issues with your request. 

#### api_credentials_invalid

User supplied an invalid API key

#####Example

```
{
    "response": {
        "status": "401",
        "message": "Unauthorized",
        "code": "api_credentials_invalid",
        "moreInfo": "https://www.tenon.io/docs/api_credentials_invalid";
    }
}
```

#### daily_limit_reached

User submitted more requests today than their plan's daily limit allows.

#####Example

```
{
    "response": {
        "status": "401",
        "message": "Unauthorized",
        "code": "daily_limit_reached",
        "moreInfo": "https://www.tenon.io/docs/daily_limit_reached";
    }
}
```

####blank_url_or_src

The parameter supplied for ‘src’ was blank/ missing and the parameter supplied for 'url' was also blank.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "blank_url_or_src",
        "moreInfo": "https://www.tenon.io/docs/blank_url_or_src";
    }
}
```

####bad_src

The parameter supplied for ‘src’ was not HTML.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "bad_src",
        "moreInfo": "https://www.tenon.io/docs/bad_src";
    }
}
```

####invalid_param

User supplied a parameter value that is outside of the expected range.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "invalid_param",
        "moreInfo": "https://www.tenon.io/docs/invalid_param";
    }
}
```

####abuse_detected

User attempted to submit an abusive request to the system.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "abuse_detected",
        "moreInfo": "https://www.tenon.io/docs/abuse_detected";
    }
}
```


####required_param_missing

A required parameter was omitted from the request

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "required_param_missing",
        "moreInfo": "https://www.tenon.io/docs/required_param_missing";
    }
}
```

####doc_source_too_big

The size of the document submitted was too big.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "doc_source_too_big",
        "moreInfo": "https://www.tenon.io/docs/doc_source_too_big";
    }
}
```

####improper_content_type

The content type detected was not a web page.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "improper_content_type",
        "moreInfo": "https://www.tenon.io/docs/improper_content_type";
    }
}
```

####url_request_failed

The URL supplied did not return a good HTTP response.

#####Example

```
{
    "response": {
        "status": "400",
        "message": "Bad Request",
        "code": "url_request_failed",
        "moreInfo": "https://www.tenon.io/docs/url_request_failed";
    }
}
```

#Understanding Issue Reports In Tenon.io API Response

**Tenon.io’s primary goal is simple: deliver accurate and relevant accessibility test results in a way that is useful and clear.** We hope that the users of our API can immediately glance at results and know what is wrong, where, and why. We hope the guidance we provide gives API users the information they need to be able to fix the issues we uncover. 

The Tenon API returns a JSON string which contains a `resultSet` node. That node will contain an array of issues. Each issue in the Tenon JSON response will contain the following keys.

## Complete issue example

All issues returned in the Tenon API response hav the following structure.

```
{    
    bpID: "25",
    certainty: "80",
    errorDescription: "Remove all tabindex values higher than zero",
    errorSnippet: "&lt;p tabindex="5"&gt; Do not use tabindex greater than zero &lt;/p&gt;",
    errorTitle: "Tabindex greater than 0",
    position: {
        column: 6,
        line: 36
    },
    priority: 82,
    ref: "https://www.tenon.io/bestpractice.php?tID=66&bpID=25",
    resultTitle: "Ensure tab order reflects expected interaction order",
    signature: "2c3eed0c6194c12f60adfbb8dc1f792a",
    standards: [
        "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 2.4.3 Focus Order"
    ],
    tID: "66",
    xpath: "/html/body/table[1]/tr[1]/td[1]/div[3]/p[1]"
}
```

Let's break this down into its individual pieces:


##bpID (Best Practice ID)

*Numeric string or integer.* The bpID maps to the specific unique ID for the best practice that was violated. Each best practice may have more than one applicable test.

Example:

`bpID: "118"`

##tID (Test ID)

*Numeric string or integer.* The tID maps to the specific unique ID for the test that was failed. 

Example: 

` tID: "3"`

##certainty

*Numeric string or integer.* This represents how confident we are that we've found an issue. This score will be one of the following:

* 0
* 20
* 40
* 60
* 80
* 100

You should expect to find very few results from Tenon which have a low certainty, but it is possible.

Example:

`certainty: "100"`

##priority

*Numeric string or integer.* This represents the calculated priority for this specific issue. The priority for an issue is calculated based on a number of factors relating to user impact, ease & speed of repair, issue location, etc.  We supply this value to assist you in determining the order in which repairs are recommended.

Example:

`priority: 73`

##errorDescription

*Alphanumeric string.* This is a description of the error and will often contain remediation guidance.

Example:

`errorDescription: "Language of the document has not been set"`

##errorSnippet

*Alphanumeric string.* This is the snippet of code found to be in violation. It is limited to 255 characters and will often contain only the HTML element with the issue. *This snippet of code will be encoded into appropriate HTML Entities.*

Example:

`errorSnippet: "&lt;html class="no-js"&gt;&lt;!--&lt;![endif]--&gt;&lt;head&gt; &lt;meta charset="utf-8"&gt; &lt;title&gt;&lt;/title&gt; &lt;style type="text/css"&gt; body { width: 1600px; margin: 0; padd",`

##errorTitle

*Alphanumeric string.* This is a concise title for the issue.

Example:

`errorTitle: "Language not set"`

##position.column

*Numeric string or integer.* This is the column of the first character of the violating code, relative to the entire document source as tested.  It does not correlate to your actual file's source code but rather the final rendered document's source.

Example:

`position: {
    column: 24,
    line: 3
},`

##position.line

*Numeric string or integer.* This is the line of the first character of the violating code, relative to the entire document source as tested. It does not correlate to your actual file's source code but rather the final rendered document's source.

Note: in very rare edge cases, such as when comments are placed above the document's DOCTYPE, this number may be off by a line or two.

Example:

`position: {
    column: 24,
    line: 3
},`

##ref

*Alphanumeric string.* ***If*** your request's 'ref' parameter was set to '1', you will see a fully qualified URL here for a more lengthy discussion of the error and more informative remediation guidance.

Example:

`ref: "https://www.tenon.io/bestpractice.php?tID3&bpID=118"
`

##resultTitle

*Alphanumeric string.* This is the specific title of the Best Practice that was violated.

Example:

`errorDescription: "Language of the document has not been set"`

##signature

*Alphanumeric string.* This is an MD5 hash of the error, to ensure uniqueness.

Example:

`signature: "44a94237c000e850648a666c8f85d30d"`

##standards

This is an array of WCAG 2.0 Success Criteria that were violated in this issue. Each array item will be an alphanumeric string.

Example:

`standards: [
     "Web Content Accessibility Guidelines (WCAG) 2.0, Level A: 3.1.1 Language of Page"
]`

##xPath

*Alphanumeric string.* This is the full xPath for the issue. As is the case with position, it does not correlate to your actual file's source code but rather the final rendered document's source.

Example:

`xpath: "/html/body/table[1]/tr[1]/td[1]/div[4]/img[1]"`

#What do I do if I disagree with a test result?

All tests for all accessibility tools are based on the tool creators’ interpretation of the relevant standards and what it means to comply with those standards.  There are two reasons why you might disagree with a test result:

* You disagree with our interpretation of the standard.
* We have a buggy test.

You have two courses of action: filter it out or tell us. We actually prefer the latter.

## Filter it out

It is impossible for us to meet the unique needs of all users and all organizations - particularly those which have created their own internal standards and defined their own risk tolerance. In such cases you may wish to filter out those results. The best and quickest way to do that is to find the Test ID (tID) or even the Best Practice ID (bpID) of the results you disagree with and filter out of future results.

We strongly recommend against sorting/ filtering of results based on the value(s) of any node that contains an alphanumeric string, such as `errorDescription`. This value is very likely to change because best practice and test information is not tied to any release cycle but is rather **likely to evolve as we see need and opportunity for the information to become more clear**.  We provide a handful of numeric items which should be used for filtering and ordering instead, such as the Best Practice ID or TestID.

Before you do that, please consider that test content should be regarded as 'fluid' in nature. Our overall goal in testing is to provide a high degree of accuracy and efficiency in testing. In general, we'd prefer that you *not* filter your results based specifically on the Test ID (tID) or Best Practice ID (bpID). Instead, we'd rather you contact us to talk about what you disagree with and why.

## Tell us

Quality is hugely important to us. If we have a bug we need to know about it. It benefits everyone - you, us, and other users - to tell us when you disagree with a test result. Here are the things we hope you can provide to make the conversation most useful:

* Report Date (responseTime)
* Report ID (reportID)
* Test ID (tID)
* Tested URL or document source, if possible
* Specific issue source (errorSnippet)
* Explanation of specifically what you disagree with and why

## What happens next:

After we get your message, we may need a while to digest it and in a lot of cases, we’ll subject it to our internal unit testing process. Improving  and correcting our tests takes time. But you can be sure we are listening and will you will receive a response within a few days.

If you find a bug in our tests, we will likely make an immediate change to our test’s logic to ensure similar errant results are no longer returned. Our tests may be modified quickly allowing Tenon to stay in step with changes to browsers, techniques and new specifications. In some  cases, the issue raised may be more a matter of opinion than a clear cut bug. Our goal, as a company, is to deliver the best possible results to all of our customers so we may choose to stand by our interpretation of the standards and make no change. In which case we will inform you of the reasoning behind our decision and if you still disagree with us, that’s fine. In such cases, we encourage you to filter out that specific tID from your results.  




#Tenon Roadmap

The Tenon Roadmap offers a glimpse into where we're going in the future. This document is neither complete nor definitive and is very likely to evolve based on demand. If you are a Tenon.io customer, give us your feedback on what you'd like to see and what you think is most important.

Last Update: May 29, 2014

## Version 1.1

The theme for 1.1 will be better stats, better tests, and increased preparation for Internationalization.

* By calculating better & more informative stats, we can offer a better glimpse into your systems' accessibility and make more informed development decisions
* Nearly 60 new tests will be added, increasing coverage for ARIA and more JavaScript-aware testing
* In terms of internationalization, nearly all of Tenon's UI strings are already translated into 8 other languages.  Additional work is needed, after which we will gauge further content translation based on demand.

The following outlines some specific new features in three different categories.

### Tenon Web Admin

* Enhanced Stats
  * Top X issues by Certainty (TEN-312)
  * % of issues by Certainty (TEN-313)
  * Top X issues by WCAG Level (TEN-311)
  * % of issues by WCAG Level (TEN-310)
  * Num. of issues by WCAG Level (TEN-309)
  * Avg. Certainty (TEN-305)
  * Avg. Priority (TEN-306)
* Add preferred language to user profile. This is a dependency for i18N work (TEN-350)
* Create an "All Best Practices" report for internal use only. The use case is to provide an ability to get a look at all BPs at once, to check for duplicates, etc. (TEN-29)
* In stats output, also provide a version that includes only today's info (TEN-340)
* Drive all emails sent by Tenon via templates. This is an important component of i18N (TEN-325, TEN-326, TEN-327)
* Refactor standards content to include a Level lookup. Current method of determining level is inefficient (TEN-317, TEN-318, TEN-319)

### Tenon Tests

* A number of new tests for valid ARIA usage
* More robust tests for JavaScript events bound accessibility.
* More mobile-web accessibility tests
* New tests for CSS-generated content and sprites on controls
* More robust & accurate tests of tables

### Tenon API

* Return min & max priority in result response (TAPI-115)
* Return min & max certainty in result response (TAPI-114)  
  
## Beyond 1.1 or at risk for backlogging

* Ability to supply an `xPath` parameter as part of request, which will limit testing only to a specific portion of the page (TAPI-2)
* Ability to log associated nodes. Use case: A test exists that will find non-unique form fields. This would allow us to also provide xPath/ line & column info for other fields with the same IDs (TEN-286)
* Add relative priority to the Best Practices Quality Report. This is an internal use report that allows us to prioritize our own efforts and improving Best Practices content (TEN-52)
* Added RESTful-ness. Our medium-to-long term goal is to move toward a more RESTful API offering more full-featured CRUD access to manage users, systems, reports, and issues.
#Tenon Application RequirementsThe key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be interpreted as described in RFC 2119. [http://www.ietf.org/rfc/rfc2119.txt](http://www.ietf.org/rfc/rfc2119.txt)##IntroductionTenon is a fast, accurate, and robust API for testing the accessibility of web-based systems.  It is fully JavaScript aware and offers a high degree of flexibility, making it – unequivocally – the most powerful accessibility testing product in existence.  Because it is an API, it can be integrated into other applications. By integrating Tenon into other applications, you can create a highly robust toolset that works for you instead of the other way around. ##Internal Use/ Personal Use ApplicationsWhen it comes to the creation and use of personal applications or implementations of Tenon for internal use, we encourage and support the use of Tenon in any manner you deem fit so long as it honors our existing license and acceptable use policies.##Integration into Third Party systems not for personal or internal useWe encourage the integration of Tenon into any other system, both free/ open-source and proprietary/ paid. There are relatively few architectural, logistical, and ethical limitations we’ll outline below. 
Following them will make us very happy and fervent supporters and collaborators. ##Your application MUST allow API key as a setting
This is more than an architectural concern, for three reasons:1.	‘key’ is a required parameter for all API requests. Requests that don’t include a valid, active key are rejected.2.	Tenon.io customers can, at any time, regenerate a new key. Doing so will delete the old key.3.	If you, as the application developer hard-codes your own key, you are then going to be charged for the usage of all of your users’ activity.##You MUST disclose the source of the test resultsYou MUST NOT attempt to obfuscate, hide, or confuse the fact that Tenon is integrated into your application:1.	Your application MUST clearly disclose and comply with TENON license.2.	You MUST NOT create derivative works in direct competition with TENON.3.	You MUST NOT remove, modify, or replace any TENON branding.4.	You MUST clearly disclose that the source of results comes from TENON5.	Pricing for paid apps MUST disclose that pricing and payment for the Tenon service is separate from the pricing and payment for the app.##Your application MUST ensure the privacy and security of your applicationTenon does automatic filtering, escaping, and validation of request parameters. We also monitor and attempt to detect abuse. If we detect abusive requests, we SHALL shut down the API key associated with those requests and MAY also backlist the IP(s) associated with those requests. ##You MUST be responsible for supporting your integrationTenon fully supports our API and the reliability, accuracy, and availability of our platform. You are responsible for supporting your application, including the portions that integrate Tenon.##You MUST retain Tenon’s flexibilityYou MUST allow the user to enter their own request parameters at least as a global setting for the app itself. Tenon’s results can vary significantly based upon the users’ request parameters. Hard coding the request parameters may cause the API to return less (or more) results than the end user expects, eroding their belief in the quality of both your product and ours. Even simply providing the parameter as global settings will return control to the users.Your application MUST NOT cache, store, remove, delete, truncate, replace or modify the error information Tenon returns, as the test information is not tied to our release cycle and MAY be revised or improved without notice. Your application MUST display all error information with each result. You MAY group identical information, for readability and ease of use. “Identical” in this case, are issues relating to the same tID in the JSON string. However, in such groupings, the code snippets, xPath, line, and column information for issues SHOULD be displayed.