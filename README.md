#READ ME

NOTE: the *only* official location for these documents is at [https://bitbucket.org/tenon-io/tenon.io-documentation](https://bitbucket.org/tenon-io/tenon.io-documentation)

We provide this open documentation repository for use by customers and developers looking for documentation on the API as a building block for their own documentation for their developed client application. We do not warrant, in any way, the quality, usability, accuracy, or suitability of derivative works.

If you plan on cloning this repository we heartily recommend you [set this repo as an upstream remote](https://help.github.com/articles/fork-a-repo#step-3-configure-remotes) and fetch upstream changes periodically.

## Generated docs

If you *just* want the generated documents, the final generated documentation can be found in the 'dist' folder. This will contain a document formatted in Markdown as well as a version that has been converted to HTML.

## Source docs

The individual source documents can be found in the 'src' folder.

## Install & Run

Because this repo already contains generated docs, you shouldn't need to do anything other than to view/ use the generated docs.  But, if for some reason you want to do something else (like add docs or convert to EPUB, too) then all you'll need to do is run:

`npm install`

Which will install the necessary Grunt plugins and then:

`grunt`

Which will generate the new docs.

## Complaints? Improvements needed?

Your comments are important to us. If the docs are confusing or need to be improved, go over to [https://bitbucket.org/tenon-io/tenon.io-documentation/issues](https://bitbucket.org/tenon-io/tenon.io-documentation/issues) and let us know. This benefits all of our users, so your contribution isn't just welcome, it is wanted.

NOTE: that is *not* where to go when you need help with the Tenon.io product itself. The URL above is *only* for the documentation itself.

