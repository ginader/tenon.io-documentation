'use strict';

module.exports = function (grunt) {
    // load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: ['Gruntfile.js']
        },

        clean: ['dist'],

        concat: {
            md: {
                files: {
                    'dist/tenon-api-developer-docs-full.md': [
                        'src/0-tenon_quickstart.md',
                        'src/1-tenon_api_developer_documentation.md',
                        'src/2-understanding-api-request-parameters.md',
                        'src/3-overview_of_the_tenon_api_response.md',
                        'src/4-understanding_response_codes.md',
                        'src/5-understanding_issue_reports_in_tenon_api_response.md',
                        'src/6-tenon_roadmap.md',
                        'src/tenon_application_requirements.md'
                    ]
                }
            }
        },

        pandoc: {
            toHtml: {
                configs: {
                    'publish' : 'HTML'
                },
                files: {
                    'from': [
                        'dist/tenon-api-developer-docs-full.md'
                    ]
                }
            }
        }
    });

    grunt.registerTask('default', ['jshint', 'clean', 'concat', 'pandoc']);
};